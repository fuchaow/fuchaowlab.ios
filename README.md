# 极客玩家Bravo Yeung的个人博客
托管并部署在 Gitlab pages上。

原地址: <https://legege007.gitlab.io>

这是我的个人博客项目，里面会记录技术、运营和生活的点点滴滴。

访问地址：<https://web.geekplayers.com>

## 大白技术控的技术博客，兼谈运营硬核技巧.

欢迎订阅公众号: 大白技术控

![大白技术控-公众号](https://cdn.jsdelivr.net/gh/yanglr/yanglr.github.io/assets/images/dotnet.jpg)
